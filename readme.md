Simple Turtlebot Learning Demo With FANN.
=========================================

To test:

CD into your ``catkin_ws/src`` directory:

    cd ~/catkin_ws/src

Download the repository:
    
    git clone https://bitbucket.org/damienjadeduff/learner_456.git

Then compile:

    cd ~/catkin_ws
    catkin_make
    
Start the simulator:

    roslaunch a1_456_referee a1.launch

or

    roslaunch turtlebot_gazebo turtlebot_world.launch

In a new terminal, start the learner:

    rosrun learner_456 learner_456_node

Now drive the robot around using teleop
(it would be a good idea to have rviz open
showing the laser scan so that you can see what the robot sees):

    roslaunch turtlebot_teleop keyboard_teleop.launch

When you are finished use ``Ctrl-C`` in the terminal to kill the learner program.
It will save a file called ``amble.net`` which contains the neural network.
That file will be saved to the current directory.

In order to sue the learnt controller now run:

    rosrun learner_456 doer_456_node

Note that this won't move the robot unless the teleop is disabled.

