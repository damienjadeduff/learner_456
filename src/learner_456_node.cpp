// Based on the learning skeleton for the Turtlebot.

// Functions
#include "ros/ros.h"
#include "geometry_msgs/Twist.h"
#include "std_msgs/Bool.h"
#include "sensor_msgs/LaserScan.h"
#include <iostream>
// #include "fann.h"
#include "floatfann.h"

ros::Subscriber sub_las_;
ros::Subscriber sub_twi_;

sensor_msgs::LaserScan laser_msg;
bool got_scan=false;

geometry_msgs::Twist cmd;

int input_size=640;

struct fann *ann;

int training_iters=0;

void laser_cb(const sensor_msgs::LaserScan::ConstPtr& msg)
{

  laser_msg=*msg; //save the laser scan message for use in learning callback
  got_scan=true;
}

void twist_cb(const geometry_msgs::Twist::ConstPtr& msg){

  if(!got_scan)return; // no perceptual data yet

  if(std::abs(msg->linear.x)<0.1 && std::abs(msg->angular.z) <0.08){
    //let's not learn if the robot is not moving fast - otherwise we will end up with a very slow robot
   return;
  }

  // ok, have laser scan and motion to remember the mapping for... let's learn something

  if(rand()%5==0){ // learn some of the time - this reduces correlation in input data
    float output[2];
    // how fast is the human making the robot drive?
    output[0] = msg->linear.x;
      // how fast is the human making the robot turn?
    output[1] = msg->angular.z;

    //clean the incoming sensory data
    for(int i=0;i<laser_msg.ranges.size();i++)
      if(std::isnan(laser_msg.ranges[i]))laser_msg.ranges[i]=0;

    //this is where the neural network gets trained:
    fann_train(ann, &(laser_msg.ranges[0]),output);
    training_iters++;

//     std::cout<<"Input: ";
//     for(int i=0;i<laser_msg.ranges.size();i++)
//       std::cout<<laser_msg.ranges[i]<<" ";
//     std::cout<<std::endl;
    std::cout<<"Desired output: linear: "<<output[0]<<" angular: "<<output[1]<<std::endl;
    std::cout<<"Training_iterations completed:"<<training_iters<<std::endl;
  }
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "amble");
    ros::NodeHandle n;

    sub_las_ = n.subscribe("/scan", 1000,laser_cb); // for perceptual data
    sub_twi_ = n.subscribe("/cmd_vel_mux/input/teleop", 1000,twist_cb); // for action demonstration

    ann = fann_create_standard(3 /*layers*/, input_size,
                               4 /*hidden neurons*/, 2 /*num output*/);

    fann_set_activation_function_hidden(ann, FANN_SIGMOID_SYMMETRIC);
    fann_set_activation_function_output(ann, FANN_SIGMOID_SYMMETRIC);
    fann_randomize_weights(ann, -0.01,0.01);

    ros::spin();

    fann_save(ann, "amble.net");

    fann_destroy(ann);

    return 0;
}
