// The learning skeleton for the Turtlebot.

// Functions
#include "ros/ros.h"
#include "geometry_msgs/Twist.h"
#include "std_msgs/Bool.h"
#include "sensor_msgs/LaserScan.h"
#include <iostream>
// #include "fann.h"
#include "floatfann.h"

ros::Subscriber sub_las_;
ros::Publisher pub_;

struct fann *ann;

void laser_cb(const sensor_msgs::LaserScan::ConstPtr& msg)
{

  sensor_msgs::LaserScan laser_msg;
  geometry_msgs::Twist cmd;
  laser_msg=*msg; //copy it because we're going to clean the data

  //clean the data
  for(int i=0;i<laser_msg.ranges.size();i++)
    if(std::isnan(laser_msg.ranges[i]))laser_msg.ranges[i]=0;

  float* output;
  output = fann_run(ann, &(laser_msg.ranges[0]));

  std::cout<<"Desired output: linear: "<<output[0]<<" angular: "<<output[1]<<std::endl;

  cmd.linear.x=output[0];
  cmd.angular.z=output[1];

  // let's make it go twice as fast --- just for fun
  cmd.linear.x*=2;
  cmd.angular.z*=2;

  pub_.publish(cmd);

}

int main(int argc, char **argv)
{
        ros::init(argc, argv, "amble");

        ros::NodeHandle n;
        pub_ = n.advertise<geometry_msgs::Twist>("/cmd_vel_mux/input/navi", 100); // for acting
        sub_las_ = n.subscribe("/scan", 1000,laser_cb); // for perceptual data

        ann = fann_create_from_file("amble.net");

        ros::spin();

        fann_destroy(ann);

        return 0;
}
